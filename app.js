#!/usr/bin/env node
const axios = require('axios');
const os = require('os');
const cheerio = require('cheerio');
const request = require('request');
const fs = require('fs')
const XLSX = require('xlsx');
const json2xls = require('json2xls');
const excel2Json = require('my-xls-to-json');
const path = require('path');
const jsonexport = require('jsonexport');
const csv2json = require('csv2json');
const csvjson = require('csvjson');
const jsonfile = require('jsonfile')



function random(count = 32) {
  const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

  let text = ''

  for (let i = 0; i < count; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length))

  return text
}

const prog = require('caporal');
prog
  .version('1.0.0')
  // you specify arguments using .argument()
  // 'app' is required, 'env' is optional

  //`nomor satu`
  .command('lowercase', 'Lowercasing application')
  .argument('<stringL>', 'String to lowercased')
  .action(function(args,options,logger){
      logger.info(args.stringL.toLowerCase())
  })
  .command('uppercase','Uppercasing application')
  .argument('<stringU>', 'String to uppercased')
  .action(function(args,options,logger){
    logger.info(args.stringU.toUpperCase())
  })
  .command('capitalize','Capitalize first string')
  .argument('<stringC>','String to Capitalized')
  .option('--tail <lines>', 'Tail <lines> lines of logs after deploy', prog.INT)
  .action(function(args, options, logger) {
      let arr = args.stringC.split(" ");
      let out = []
      arr.forEach(element => {
          out.push(element[0].toUpperCase() + element.slice(1));
      });
      out = out.join(" ")
      logger.info(out)
  })
    //`nomor dua`
  .command('add','sum all input')
  .argument('<input...>','input to sumarize')
  .action(function(args,options,logger){
      let result = 0;
      args.input.
      forEach(element => {
        result += parseInt(element)    
      }); 
        logger.info("ini result = " + result);
})

.command('subtract','subtract all input')
.argument('<input...>','input to sumarize')
.action(function(args,options,logger){
    let result = parseInt(args.input);
    args.input.slice(1).
    forEach(element => {
      result -= parseInt(element)    
    }); 
      logger.info("ini result = " + result);
})

.command('multiply','multiply all input')
.argument('<input...>','input to sumarize')
.action(function(args,options,logger){
    let result = 1;
    args.input.
    forEach(element => {
      result *= parseInt(element)    
    }); 
      logger.info("ini result = " + result);
})

.command('divide','divide all input')
.argument('<input...>','input to sumarize')
.action(function(args,options,logger){
    let result = parseInt(args.input);
    args.input.slice(1).
    forEach(element => {
      result /= parseInt(element)    
    }); 
      logger.info("ini result = " + result);
})

        //`nomor tiga`

.command('palindrome','check if input palindrome or not')
.argument('<input>','input to sumarize')
.action(function(args,options,logger){
    if (args.input.toLowerCase().split("").reverse().join("")==args.input.toLowerCase()){
        return logger.info('Yes')
    }
    logger.info('No')
})

        //nomor empat
// .command('obfuscate','')

        /*nomor lima*/
// .command('random','randomalphanumeric')
// .option('--length <length>', 'Number of pizza', random('<length>'),1)
// .option('--letters <letter>','without letter',random())
// .action(function(args,options,logger){
//   if (options.number.length){
//     logger.info(random(options.number))
//   }else{
//     logger.info(random())
//   }
//   // logger.info(random(options.number))
// })

      //nomor enam privat
.command('ip')
.action(async function(logger){
  let respon = os.networkInterfaces()
  console.log(respon.wlp3s0[0].address)
})
  

      //Nomor tujuh external 
.command('ip-external')
.action( async function(args,options,logger){
  let respon = await axios.get("http://freegeoip.net/json/")
  logger.info(respon.data.ip)
})

      //nomor delapan
.command('headlines')
.action(function(args,options,logger){
  request('https://kompas.com/', function (error, response, html) {
  if (!error && response.statusCode == 200) {
    var $ = cheerio.load(html);
    // let a = $('.headline__thumb__item').nextAll().children('.headline__thumb__link');
    // console.log(a.attr('href'))
    // logger.info(a)
    // let a = $('.headline').text()
    // console.log('ini a= '+a)
    $('.headline__thumb__item').each( (index, value) => {
      console.log(value+index)
      let link = $(value).children('.headline__thumb__link').attr('href');
      let text = $(value).children().children('.headline__thumb__title').text();
      console.log('\n'+ 'Title: ' +text+'\n'+ 'URL: ' +link);
      ;
   });
  }
});
})

      //nomor sembilan
.command('convert')
.argument('<input...>','masukan file yang akan diubah dan format akhir yang diinginkan')
.action(function(args,option,logger){
  let dir = __dirname;
  excel2Json(path.join(dir,'test.xls'), function(err, output) {
    // output is the JSON Object. 
    let store = output.test
    console.log('ini store = \n'+ JSON.stringify(store));
    console.log(store);
    
    
    jsonexport(store[0],function(err, csv){
      if(err) return console.log(err);
      console.log(csv);
      fs.writeFile('csv-test2.csv',csv)
  });
  // fs.createReadStream('csv-test.csv')
  // .pipe(csv2json({
  //   // Defaults to comma.
  //   separator: ':'
  // }))
  // .pipe(fs.createWriteStream('data.json'));
  let data = fs.readFileSync(path.join(__dirname, 'csv-test.csv'), { encoding : 'utf8'});
  let options = {
    delimiter : ',', // optional
    quote     : '"' // optional
  };
  let a = csvjson.toObject(data, options);
  console.log('ini a :');
  let jzon = a[0]
  console.log(a[0]);
  
  jsonfile.writeFile('data3.json',a[0])
  let xls = json2xls(a[0]);

  fs.writeFileSync('data.xlsx', xls, 'binary');
  let dat2a = fs.readFileSync(path.join(__dirname, 'data3.json'), { encoding : 'utf8'});
  let options2 = {
    delimiter   : ",",
    wrap        : false
}
  let hasilcsv = csvjson.toCSV(jzon, options);
  console.log('ini dari json ke csv yang kedua');
  console.log(hasilcsv);
  
  
});
})
      //nomor sembilan

.command('screenshot')
.action(function(args,option,logger){
  const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://google.com');
  await page.screenshot({path: 'example.png'});

  await browser.close();
})();
})
prog.parse(process.argv);
